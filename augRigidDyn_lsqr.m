%% Least squares - Augmented Rigid Link Approach
% Calculate paramaters to fit recorded data
%% Input
%    q       : angle (deg)
%    q_vel   : angular velocity (deg/s)
%    q_accel : angular acceleration (deg/s^2)
%    p       : pressure (Pa)
%    M       : nxn inertia matrix (n=1)
%    C       : nxn coriolis and centrifugal matrix
%    G       : nx1 gravitational matrix 
%% Output
%    alpha   : torque-pressure mapping (Nm/Pa)
%    d       : damping constant (kg/s)
%    k       : spring constant (N/m)
    
function [alpha, d, k] = augRigidDyn_lsqr(q, q_vel, q_accel, p, M, C, G)
    % Convert to radians
    q = deg2rad(q);
    q_vel = deg2rad(q_vel);
    q_accel = deg2rad(q_accel);
    
    % Least squares Ax = b
    b = -M.*q_accel - C.*q_vel - G;
    A = [q q_vel -p];
    
    % lsqrnonneg prevents negative estimations 
    estimated_parameters = lsqnonneg(A,b);
    
    k = estimated_parameters(1); % spring constant (N/m)
    d = estimated_parameters(2); % damping constant (kg/s)
    alpha = estimated_parameters(3); % torque-pressure mapping (Nm/Pa)
end
