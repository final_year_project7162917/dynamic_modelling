%% Computationally Effecient Energy Based Approach
% Computationally efficient energy based dynamic modelling approach of a 
% soft finger.
% Modelling method described in: https://www.sciencedirect.com/science/article/pii/S0957415818301715
%% Inputs
%    mass    : mass (kg)
%    length  : length (m)
%    q       : angle (deg)
%    q_vel   : angular velocity (deg/s)
%    q_accel : angular acceleration (deg/s^2)
%% Outputs
%    torque  : torque (N/m)
%    M       : nxn inertia matrix (n=1)
%    C       : nxn coriolis and centrifugal matrix
%    G       : nx1 gravitational matrix 

function [torque, M, C, G]= compEffDyn(mass, length, q, q_vel, q_accel)
    % Constants
    g = 9.81; % Gravity (m/s^2)
    
    % Convert to radians
    q = deg2rad(q);
    q_vel = deg2rad(q_vel);
    q_accel = deg2rad(q_accel);
    
    % Setup dynamic equations as in Equation 52 of the report 
    M = mass * length^2 * (1/20 - q^2/504);
    C = - ((mass * length^2) / 504) * q * q_vel;
    G = (mass * g * length / 12) * q;
    
    torque = M * q_accel + C * q_vel + G;
end