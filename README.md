# Dynamic Modelling
This repository contains the computationally efficient energy and augmented rigid link dynamic modelling approaches and the least-squares estimation method for the parameters of each model.


## compEffDyn.m
Computationally efficient energy based dynamic modelling approach of a soft finger using the method described in: \
https://www.sciencedirect.com/science/article/pii/S0957415818301715.

## augRigidDyn.m
Augmented rigid link dynamic modelling of a soft finger using the method described in: https://ieeexplore.ieee.org/document/8722799.

## lagrangeFinder.m
Determine the lagrangian and dynamics equation of a point mass.

## calculateDynamicParameters.m
Use experimental data to calculate least-square estimation of the parameters required for the computationally efficient energy based and augmented rigid link models. 

## augRigidDyn_lsqr.m
Calculate augmented rigid link paramaters to fit experimental data.

## compEffDyn_lsqr.m
Calculate computationally efficient energy paramaters to fit experimental data.


