%% augRigidDyn.m
% Augmented rigid link dynamic modelling of a soft finger.
% Modelling method described in: https://ieeexplore.ieee.org/document/8722799
%% Inputs
%    mass    : mass (kg)
%    length  : length (m)
%    iz      : rotational inertia (kg.m^2)
%    q       : angle (deg)
%    q_vel   : angular velocity (deg/s)
%    q_accel : angular acceleration (deg/s^2)
%% Outputs
%    torque  : torque (N/m)
%    M       : 1x1 inertia matrix 
%    C       : 1x1 coriolis and centrifugal matrix
%    G       : 1x1 gravitational matrix 

function [torque, M, C, G]= augRigidDyn(mass, length, iz, q, q_vel, q_accel, K, D)
    % Constants 
    g = 9.81; % Gravity (m/s^2)
    
    % Convert Constant Curvature (CC) angles to radians 
    q = deg2rad(q);
    q_vel = deg2rad(q_vel);
    q_accel = deg2rad(q_accel);
    
    % Create the mapping matrix, ξ = m(q) = [theta1, d2, d3, theta4]
    augLinks_pos = cc2rigid(q, length);
    
    % Defined Jacobian 
    [Jm, Jm_vel] = rigidJacobian(q, length, q_vel);
    
    % Find ξ_dot
    augLinks_vel = Jm * q_vel;
    
    % Find ξ_accel = [theta1_accel, d2_accel, d3_accel, theta4_accel]
    augLinks_accel = Jm_vel * q_vel + Jm * q_accel;
    
    % Setup M_ξ, C_ξ, G_ξ augmented matricies
    M1 = [2*(mass*augLinks_pos(2)^2 + iz)*augLinks_vel(1); 0; 0; 0];    
    C1 = [2*mass*augLinks_pos(2)*augLinks_vel(2) * augLinks_vel(1); 0; 0; 0]; 
    G1 = mass*g*augLinks_pos(2)*cos(augLinks_pos(1));
    
    M2 = [0; mass; 0; 0];
    C2 = [-mass*augLinks_pos(2)*augLinks_vel(1); 0; 0; 0];
    G2 = mass*g*sin(augLinks_pos(1));
    
    M3 = [0; 0; 0; 0];
    C3 = [0; 0; 0; 0];
    G3 = 0;
    
    M4 = [0; 0; 0; 0];
    C4 = [0; 0; 0; 0];
    G4 = 0;
    
    Maug = [M1 M2 M3 M4];
    Caug = [C1 C2 C3 C4];  
    Gaug = [G1; G2; G3; G4];
    
    % Map augmented rigid link dynamics to CC dynamics 
    % See Equation 43 of the report
    M = Jm' * Maug * Jm;
    C = Jm' * Maug * Jm_vel + Jm' * Caug * Jm; 
    G = Jm' * Gaug;

    % Find torque
    torque = M*q_accel + (C + D)*q_vel + G + K;
end

% Determine the augmented jacboian 
function [Jm, Jm_vel] = rigidJacobian(q, q_vel, L)
    % Setup Jacobian as in Equation 38 of the report
    J_theta1 = 1/2;
    J_theta4 = 1/2;
    J_d2 = L * (q * cos(q/2) - 2 * sin(q/2)) / q^2;
    J_d3 = J_d2;

    Jm = [J_theta1; J_d2; J_d3; J_theta4];

    % Setup differentiated jacobian as in Equation 39 of the report.
    lci_vel = L * ((-q_vel*sin(q/2) - 2*q_vel*cos(q/2)) / 2*q ...
                + (-q*q_vel*cos(q/2) + 4*sin(q/2)*q_vel) / q^3);

    Jm_vel = [0; lci_vel; lci_vel; 0];
end

% Define mapping between CC arc and rigid links
% Inputs
%    q  : bending angle of CC arc (rad)
% Outputs
%    l  : length of CC arc (m)

function [augLinks_pos] = cc2rigid(q, l)
    % Setup m(q) as in Equation 36 of the report.
    theta1 = q/2;
    d2 = l * sin(q/2) / q;
    d3 = d2;
    theta4 = q/2;

    augLinks_pos = [theta1 d2 d3 theta4];
end






