%% Least squares - Computationally Efficient Energy Approach
% Calculate paramaters to fit recorded data
%% Input
%    q_vel   : angular velocity (deg/s)
%    q_accel : angular acceleration (deg/s^2)
%    p       : pressure (Pa)
%    M       : nxn inertia matrix (n=1)
%    C       : nxn coriolis and centrifugal matrix
%    G       : nx1 gravitational matrix 
%% Output
%    alpha   : torque-pressure mapping (Nm/Pa)
    
function alpha = compEffDyn_lsqr(q_vel, q_accel, p, M, C, G)
    % Convert to radians
    q_vel = deg2rad(q_vel);
    q_accel = deg2rad(q_accel);
    
    % Least squares Ax = b
    b = -M.*q_accel - C.*q_vel - G;
    A = -p;
    
    % lsqrnonneg prevents negative estimations 
    estimated_parameters = lsqnonneg(A,b);

    alpha = estimated_parameters(1); % torque-pressure mapping (Nm/Pa)
end
