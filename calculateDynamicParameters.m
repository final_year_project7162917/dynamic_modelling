%% calculateDynamicParamaters.m
% Use experimental data to calculate least-square estimation of the
% parameters required for the computationally efficient energy based and
% augmented rigid link models. 

close all;

%% Open angle and pressure data
angle = load('example_data/ex_angle_step_60k.mat');
pressure = load('example_data/ex_p_step_60k.mat');

q = angle.angle;
q_vel = angle.angle_diff_f;
q_accel = angle.angle_ddiff_f;
t = angle.t;

p = pressure.p_aligned;
p_t = pressure.t;

%% Calculate Dynamics: M, C and G values at each timestep 
m = 0.04; % Finger mass (kg)
l = 0.079; % Finger length (m)
iz = 8.136e-6; % Finger inertia around pivot axis (kg.m^2)

rigid_M = zeros(length(q),1); 
rigid_C = zeros(length(q),1); 
rigid_G = zeros(length(q),1); 
rigid_tau = zeros(length(q),1);
energy_M = zeros(length(q),1); 
energy_C = zeros(length(q),1); 
energy_G = zeros(length(q),1); 
energy_tau = zeros(length(q),1); 

% Loop through each timestep and calculate dynamics at each step
for i = 1:length(t)
    [tau, M, C, G] = augRigidDyn(m, l, iz, q(i), q_vel(i), q_accel(i), 0, 0);
    rigid_M(i) = M;
    rigid_C(i) = C;
    rigid_G(i) = G;
    rigid_tau(i) = tau;
    
    [tau, M, C, G]= compEffDyn(m, l, q(i), q_vel(i), q_accel(i));
    energy_M(i) = M;
    energy_C(i) = C;
    energy_G(i) = G;
    energy_tau(i) = tau;
end

%% Least Square estimation
e_alpha = compEffDyn_lsqr(q_vel, q_accel, p, energy_M, energy_C, energy_G);
[r_alpha, r_D, r_K] = augRigidDyn_lsqr(q, q_vel, q_accel, p, rigid_M, rigid_C, rigid_G);

%% Recalc rigid dynamics with K and D parameters 
for i = 1:length(q)
    [rigid_tau(i), ~, ~, ~] = augRigidDyn(m, l, iz, q(i), q_vel(i), q_accel(i), r_K, 0);
end

%% Plot
figure(1)

hold on;

plot(t, energy_tau.*(1/e_alpha) / 1e3, 'LineWidth', 1, 'Color', [0 0.4470 0.7410 0.5]);
plot(t, rigid_tau.*(1/r_alpha) / 1e3, 'LineWidth', 1, "Color", [0.8500 0.3250 0.0980 0.5]); % 1.5e-6
plot(t, p / 1e3, 'LineWidth', 1.5, 'Color', [0.4660 0.6740 0.1880]);

ylabel("Pressure (kPa)", 'FontSize', 15);
xlabel("Time (s)", 'FontSize', 15);

xlim([0 13.5])
ylim([0 90])
legend('Computationally Efficient Energy Method',...
    'Augmented Rigid Link Approach','Actual Pressure', 'FontSize', 11.5);
set(gca,'linewidth',1)

hold off;
