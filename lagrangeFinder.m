%% lagrangeFinder.m
% Determine the lagrangian and dynamics equation of a point mass.

% Setup joint variables, mass (m), intertia(I) and gravity(g)
syms theta1(t) d2(t) d3(t) theta4(t) m I g

% Point mass position in xy coordinate system
x = d2*cos(theta1);
y = d2*sin(theta1);

% Mass velocity variables in xy coordinate system
y_vel = diff(x,t);
x_vel = diff(y,t);

% Determine joint velocities
theta1_dot = diff(theta1, t);
d2_dot = diff(d2, t);
d3_dot = diff(d3, t);
theta4_dot = diff(theta4, t);

% Determine Lagrangian 
KE = 0.5*m*(x_vel^2 + y_vel^2) + 0.5 * I * theta1_dot^2; % Kinetic energy
PE = m*g*d2*sin(theta1); % Potential energy
L = KE - PE;

% Determine dynamic equations using the Lagrange equation
dynamic_eq = solveLagrange(L, theta1, d2, d3, theta4);

% Display equations. Four equations due to four joint variables. 
disp(dynamic_eq{1})
disp(dynamic_eq{2})
disp(dynamic_eq{3})
disp(dynamic_eq{4})

function [dynamic_eq] = solveLagrange(L, varargin)
    % Setup time variable
    syms t

    % Number of dynamic equations = number of joint variables
    dynamic_eq = cell(length(varargin),1);  

    % Determine dynamic equation for each joint
    for idx = 1:length(varargin)
        % Get joint variable and velocity
        joint = varargin{idx};
        joint_dot = diff(joint, t);

        % Find lagrange equation d/dt(dL/djoint_dot) - dL/djoint 
        dynamic_eq{idx} = simplify(diff(diff(L, joint_dot), t) - diff(L, joint));
    end     
end
